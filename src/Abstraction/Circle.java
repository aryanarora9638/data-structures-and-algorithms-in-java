package Abstraction;

public class Circle extends Shape{
    private int radius;
    final static double pi = 3.14;

    public Circle(String color, int radius){
        super(color);
        this.radius = radius;
    }

    public void getArea(){
        System.out.println("Circle area is : " + (pi*this.radius*this.radius));
    }
}
