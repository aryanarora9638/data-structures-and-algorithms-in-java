package Abstraction;

public class Rectangle extends Shape {
    private int length;
    private int breadth;

    public Rectangle(String color, int length, int breadth){
        super(color);
        this.breadth = breadth;
        this.length = length;
    }

    public void getArea(){
        System.out.println("Area of rectangle is : " + (this.length*this.breadth));
    }
}
