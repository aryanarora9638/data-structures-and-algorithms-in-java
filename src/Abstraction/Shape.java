package Abstraction;

public abstract class Shape {
    private String color;

    public Shape(String color){
        this.color = color;
    }

    public void draw(){
        System.out.println("Shape color : " + this.color);
    }

    abstract void getArea();
}
