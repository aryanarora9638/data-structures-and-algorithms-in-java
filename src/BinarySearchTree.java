public class BinarySearchTree {
    private Node root;
    private Node pointer;

    public BinarySearchTree(){
        this.root = null;
        this.pointer = null;
    }

    public void insertNode(int item){
        if(this.pointer == null){
            Node newNode = new Node(null, item, null);
            this.root = newNode;
            return;
        }
        if(item <= this.pointer.value){
            this.pointer = this.pointer.leftChild;
            insertNode(item);
        }
        else {
            this.pointer = this.pointer.rightChild;
            insertNode(item);
        }
    }

    public void searchNode(int item){
        if(this.pointer == null){
            return;
        }
        else if(this.pointer.value == item){
            return;
        }
        else if (item <= this.pointer.value){
            this.pointer = this.pointer.leftChild;
            this.searchNode(item);
        }
        else{
            this.pointer = this.pointer.rightChild;
            this.searchNode(item);
        }
    }


    class Node {
        private Node leftChild;
        private int value;
        private Node rightChild;

        public Node(){
            this.rightChild = null;
            this.leftChild = null;
            this.value = -1;
        }

        public Node(Node leftChild, int value, Node rightChild){
            this.rightChild = rightChild;
            this.leftChild = leftChild;
            this.value = value;
        }

    }
}
