public class BinaryTree {
    private Node root;

    public BinaryTree(){
        this.root = null;
    }

    public void insertNode(int item){
        Node newNode = new Node(null,item,null);
        if(root == null){
            this.root = newNode;
        }

    }

    class Node {
        private Node leftChild;
        private int value;
        private Node rightChild;

        public Node(){
            this.rightChild = null;
            this.leftChild = null;
            this.value = -1;
        }

        public Node(Node leftChild, int value, Node rightChild){
            this.rightChild = rightChild;
            this.leftChild = leftChild;
            this.value = value;
        }

    }
}
