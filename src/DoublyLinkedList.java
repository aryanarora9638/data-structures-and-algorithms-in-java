public class DoublyLinkedList {
    private Node head;
    private Node tail;

    public DoublyLinkedList(int firstItem){
        Node newNode = new Node();

        this.head = newNode;
        this.tail = newNode;

        newNode.previousLink = this.head;
        newNode.value = firstItem;
        newNode.nextLink = this.tail;
    }

    public void insertAtStart(int item){
        Node newNode = new Node(this.head.previousLink, item, this.head);
        this.head.previousLink = newNode;
        this.head = newNode;
    }

    public void removeFromStart(){
       Node pointer = this.head;
       this.head = pointer.nextLink;
       this.head.previousLink = this.head;

       pointer.nextLink = null;
       pointer.previousLink = null;
    }

    public void insertAtEnd(int item){
        Node newNode = new Node(this.tail, item, this.tail.nextLink);
        this.tail.nextLink = newNode;
        this.tail = newNode;
    }

    public void removeFromEnd(){
        Node pointer = this.tail;
        this.tail = pointer.previousLink;
        this.tail.nextLink = this.tail;

        pointer.nextLink = null;
        pointer.previousLink = null;
    }

    public void removeAtItemValue(int item){
        Node pointer_a = this.head;
        Node pointer_b = this.head;

        while (pointer_a.value != item){
            pointer_b = pointer_a;
            pointer_a = pointer_a.nextLink;
        }
        //pointer a points at the node to be removed
        pointer_b.nextLink = pointer_a.nextLink;
        pointer_a.nextLink.previousLink = pointer_b;

        pointer_a.nextLink = null;
        pointer_a.previousLink = null;
    }

    public void printLinkedList(){
        Node pointer = this.head;
        while (pointer != this.tail){
            System.out.println(pointer.value);
            pointer = pointer.nextLink;
        }
    }

    //sort linked list using bubble sort
    public void sortLinkedList(){
        boolean isSorted = false;
        while (!isSorted){
            Node pointer = this.head;
            isSorted = true;
            while (pointer.nextLink.nextLink != this.tail){
                if(pointer.value > pointer.nextLink.value){
                    swapNodesValues(pointer, pointer.nextLink);
                    isSorted = false;
                }
                pointer = pointer.nextLink;
            }
        }
    }


    public void swapNodesValues(Node first, Node second){
        //swap value
        int temp = first.value;
        first.value = second.value;
        second.value = temp;
    }


    class Node {
        private int value;
        private Node nextLink;
        private Node previousLink;

        public Node(){
            this.value = -1;
            this.nextLink = this.previousLink = null;
        }

        public Node(Node newPreviousLink, int newValue , Node newNextLink){
            this.value = newValue;
            this.nextLink = newNextLink;
            this.previousLink = newPreviousLink;
        }
    }
}
