package Encapsulation;

public class Employee {
    private String name;
    private int age;
    public static int totalEmployees;

    public Employee(String name, int age){
        this.age = age;
        this.name = name;
        totalEmployees += 1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static int getTotalEmployees() {
        return totalEmployees;
    }

    public static void setTotalEmployees(int totalEmployees) {
        Employee.totalEmployees = totalEmployees;
    }
}
