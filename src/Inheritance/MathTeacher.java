package Inheritance;

public class MathTeacher extends Teacher {
    private int favoriteNumber;

    public MathTeacher(String name, int age, int favoriteNumber){
        super(name,age);
        this.favoriteNumber = favoriteNumber;
    }

    public int getFavoriteNumber() {
        return favoriteNumber;
    }

    public void setFavoriteNumber(int favoriteNumber) {
        this.favoriteNumber = favoriteNumber;
    }
}
