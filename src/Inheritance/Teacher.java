package Inheritance;

public class Teacher {
    private String name;
    private int age;
    public static int totalTeachers;

    public Teacher(String name, int age) {
        this.name = name;
        this.age = age;
        Teacher.totalTeachers += 1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static int getTotalTeachers() {
        return totalTeachers;
    }

    public static void setTotalTeachers(int totalTeachers) {
        Teacher.totalTeachers = totalTeachers;
    }

}
