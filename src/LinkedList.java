public class LinkedList {
    private Node head;

    public LinkedList(int item){
        this.head = new Node();
        head.link = null;
        head.value = item;
    }


    public void insertAtStart(int item){
        Node newNode = new Node(item,this.head);
        this.head = newNode;
    }

    public void removeFromStart(){
        Node pointer = this.head;
        this.head = pointer.link;
    }

    public void insertAtEnd(int item){
        Node pointer = this.head;
        //reach the end
        while (pointer.link != null){
            pointer = pointer.link;
        }
        //now pointer is at the end
        Node newNode = new Node(item,null);
        pointer.link = newNode;
    }

    public void removeFromEnd(){
        Node pointer = this.head;
        while (pointer.link.link != null){
            pointer = pointer.link;
        }
        //stops one before the last one
        pointer.link = null;
    }

    public void removeAtItemValue(int item){
        Node pointer_a = this.head;
        Node pointer_b = this.head;
        while (pointer_a.value != item){
            pointer_b = pointer_a;
            pointer_a = pointer_a.link;
        }
        //pointer_a points to the node to be removed
        pointer_b.link = pointer_a.link;
        pointer_a.link = null;
    }

    public void printLinkedList(){
        Node pointer = this.head;
        System.out.print("[");
        while (pointer != null){
            System.out.print(pointer.value + ",");
            pointer = pointer.link;
        }
        System.out.print("\b]");
    }

    //sort linked list using bubble sort
    public void sortLinkedList(){
        boolean isSorted = false;
        while (!isSorted){
            Node pointer_a = this.head;
            isSorted = true;
            while(pointer_a.link.link != null){
                if(pointer_a.value > pointer_a.link.value){
                    swapNodesValues(pointer_a, pointer_a.link);
                    isSorted = false;
                }
                pointer_a = pointer_a.link;
            }
        }
    }


    public void swapNodesValues(Node first, Node second){
        //swap value
        int temp = first.value;
        first.value = second.value;
        second.value = temp;
    }


    class Node {
        private int value;
        private Node link;

        public Node(){
            this.value = -1;
            this.link = null;
        }

        public Node(int newValue, Node newLink){
            this.value = newValue;
            this.link = newLink;
        }
    }
}
