import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String args[]){
        int size = 10;

////        Stack
//        Stack stack = new Stack(size);
//        for (int i = 0 ; i < size+4 ; i++){
//            stack.push(i+1);
//        }
//        stack.showStack();
//
//        Person person1 = new Person("sample1", 13);
//        Person person2 = new Person("sample2", 17);
//
//        PersonStack personStack = new PersonStack(size);
//        personStack.push(person1);
//        personStack.push(person2);
//
//        personStack.showStack();
//
//
////        Queue
//        Queue queue = new Queue(size);
//        for (int i = 0 ; i < size+10 ; i++){
//            queue.enqueue(i+1);
//        }
//        queue.showQueue();

//        queue.dequeue();
//        queue.dequeue();
//        queue.dequeue();
//
//        queue.showQueue();
//
//        queue.enqueue(21);
//        queue.enqueue(22);
//        queue.enqueue(23);
//
//        queue.showQueue();

//        LinkedList
//        int firstItem = 3;
//        LinkedList linkedList = new LinkedList(firstItem++);
//        linkedList.insertAtStart(firstItem++);
//        linkedList.insertAtStart(firstItem++);
//        linkedList.insertAtEnd(firstItem++);
//        linkedList.insertAtEnd(firstItem++);
//
//        linkedList.printLinkedList();
//
//        linkedList.sortLinkedList();
//        System.out.println("===sort linked list using bubble sort===");
//        linkedList.printLinkedList();
//
//        System.out.println("Linked list ->");
//        linkedList.printLinkedList();
//
//        linkedList.removeFromStart();
//        System.out.println("===remove from start===");
//        linkedList.printLinkedList();
//
//        linkedList.removeFromEnd();
//        System.out.println("===remove from end===");
//        linkedList.printLinkedList();
//
//        //linkedList.removeAtItemValue(3);
//        System.out.println("===remove at item(3)===");
//        linkedList.printLinkedList();
//
//        linkedList.sortLinkedList();
//        System.out.println("===sort linked list using bubble sort===");
//        linkedList.printLinkedList();

//        DoublyLinkedList
//        int firstItem = 3;
//        DoublyLinkedList doublyLinkedList = new DoublyLinkedList(firstItem++);
//        doublyLinkedList.insertAtStart(firstItem++);
//        doublyLinkedList.insertAtStart(firstItem++);
//        doublyLinkedList.insertAtEnd(firstItem++);
//        doublyLinkedList.insertAtEnd(firstItem++);
//        doublyLinkedList.insertAtEnd(firstItem++);
//
//        doublyLinkedList.printLinkedList();
//
//        doublyLinkedList.sortLinkedList();
//        System.out.println("===sort linked list using bubble sort===");
//        doublyLinkedList.printLinkedList();
//
//        System.out.println("Linked list ->");
//        doublyLinkedList.printLinkedList();
//
//        doublyLinkedList.removeFromStart();
//        System.out.println("===remove from start===");
//        doublyLinkedList.printLinkedList();
//
//        doublyLinkedList.removeFromEnd();
//        System.out.println("===remove from end===");
//        doublyLinkedList.printLinkedList();
//
//        doublyLinkedList.removeAtItemValue(5);
//        System.out.println("===remove at item(5)===");
//        doublyLinkedList.printLinkedList();
//
//        doublyLinkedList.sortLinkedList();
//        System.out.println("===sort linked list using bubble sort===");
//        doublyLinkedList.printLinkedList();

//        BinarySearchTree
//        BinarySearchTree binarySearchTree = new BinarySearchTree();
//        binarySearchTree.insertNode(23);
//        binarySearchTree.insertNode(21);
//        binarySearchTree.insertNode(25);


        //HashMap
//        Map<String, String> map = new HashMap<>();
//        map.put("aryan", "arora");
//        map.put("shreya", "arora");
//        System.out.println(map.get("aryan"));
//
//        Map<String, Integer> map1 = new HashMap<>();
//        map1.put("aryan", 326);
//        map1.put("shreya", 347);
//        map1.put("aryan1", 328);
//        map1.put("shreya1", 349);
//        map1.put("aryan2", 350);
//        map1.put("shreya2", 351);
//        //getting all the values
//        //M-1
//        //get all the keys
//        Set<String> key = map1.keySet();
//        System.out.println();
//        for(String e : key){
//            System.out.print(map1.get(e) + " , ");
//        }
//        //M-2
//        //get all the entry set
//        Set<Map.Entry<String, Integer>> entries = map1.entrySet();
//        System.out.println();
//        for(Map.Entry<String, Integer> e : entries){
//            System.out.println(e.getKey() + " " + e.getValue());
//        }
//
//        String a = "aryan";
//        System.out.println(a.hashCode()%10);
        _HashMap hashMap = new _HashMap();
        for(int i = 1; i < 17; i++){
            hashMap.addElement(i);
        }
        for(int i = 1; i < 17; i++){
            hashMap.removeElement(i);
        }

    }
}
