package ParkingLot;

public class Bike extends Vehicle {
    private boolean isParked;
    private String size;

    public Bike(String vehicleType, boolean isHandicaped){
        super(vehicleType, isHandicaped);
        this.size = "Small";
    }

    public boolean isParked() {
        return isParked;
    }

    public void setParked(boolean parked) {
        isParked = parked;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
