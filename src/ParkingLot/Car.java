package ParkingLot;

public class Car extends Vehicle {
    private boolean isParked;
    private String size;

    public Car(String vehicleType, boolean isHandicaped){
        super(vehicleType, isHandicaped);
        this.size = "Medium";
    }

    public boolean isParked() {
        return isParked;
    }

    public void setParked(boolean parked) {
        isParked = parked;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
