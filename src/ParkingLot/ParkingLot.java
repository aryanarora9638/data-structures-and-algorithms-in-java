package ParkingLot;

import java.nio.file.Path;

public class ParkingLot {
    private int smallRegular;
    private int smallHandicaped;//Handicapped

    private int mediumRegular;
    private int mediumHandicaped;

    private int largeRegular;
    private int largeHandicaped;

    public ParkingLot(int smallRegular, int smallHandicaped,
                      int mediumRegular, int mediumHandicaped,
                      int largeRegular, int largeHandicaped){
        this.smallRegular = smallRegular;
        this.smallHandicaped = smallHandicaped;

        this.mediumRegular = mediumRegular;
        this.mediumHandicaped = mediumHandicaped;

        this.largeRegular = largeRegular;
        this.largeHandicaped = largeHandicaped;
    }

    public void Park(Vehicle vehicle){
    }

    public int getSmallRegular() {
        return smallRegular;
    }

    public void setSmallRegular(int smallRegular) {
        this.smallRegular = smallRegular;
    }

    public int getSmallHandicaped() {
        return smallHandicaped;
    }

    public void setSmallHandicaped(int smallHandicaped) {
        this.smallHandicaped = smallHandicaped;
    }

    public int getMediumRegular() {
        return mediumRegular;
    }

    public void setMediumRegular(int mediumRegular) {
        this.mediumRegular = mediumRegular;
    }

    public int getMediumHandicaped() {
        return mediumHandicaped;
    }

    public void setMediumHandicaped(int mediumHandicaped) {
        this.mediumHandicaped = mediumHandicaped;
    }

    public int getLargeRegular() {
        return largeRegular;
    }

    public void setLargeRegular(int largeRegular) {
        this.largeRegular = largeRegular;
    }

    public int getLargeHandicaped() {
        return largeHandicaped;
    }

    public void setLargeHandicaped(int largeHandicaped) {
        this.largeHandicaped = largeHandicaped;
    }
}
