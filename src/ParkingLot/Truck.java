package ParkingLot;

public class Truck extends Vehicle {
    private boolean isParked;
    private String size;

    public Truck(String vehicleType, boolean isHandicaped){
        super(vehicleType, isHandicaped);
        this.size = "Large";
    }

    public boolean isParked() {
        return isParked;
    }

    public void setParked(boolean parked) {
        isParked = parked;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
