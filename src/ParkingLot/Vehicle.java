package ParkingLot;

public class Vehicle {
    private String vehicleType;
    private boolean isHandicaped;

    public Vehicle(){
        this.vehicleType = "empty";
        this.isHandicaped = false;
    }

    public Vehicle(String vehicleType, boolean isHandicaped){
        this.vehicleType = vehicleType;
        this.isHandicaped = isHandicaped;
    }


    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public boolean isHandicaped() {
        return isHandicaped;
    }

    public void setHandicaped(boolean handicaped) {
        isHandicaped = handicaped;
    }
}
