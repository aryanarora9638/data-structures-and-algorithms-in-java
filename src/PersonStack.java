public class PersonStack {
    private Person [] stack;
    private int top;
    private int size;

    public PersonStack(){
        top = -1;
        size = 100;
        stack = new Person[size];
    }

    public PersonStack(int size){
        top = -1;
        this.size = size;
        stack = new Person[size];
    }

    public void push(Person item){
        if (!this.isFull()){
            this.top += 1;
            this.stack[this.top] = item;
        }
        else {
            System.out.println("Stack is full already");
            createNewStack(item);
        }
    }

    private void createNewStack(Person newItem) {
        PersonStack newStack = new PersonStack(this.size*2);
        for (Person item : this.stack){
            newStack.push(item);
        }
        this.stack = newStack.stack;
        this.size = newStack.size;
        this.top = newStack.top;
        System.out.println("New stack created with 2x the size");
    }

    public void pop(){
        if(!this.isEmpty()){
            this.stack[this.top] = new Person("aryan", 21);
            this.top += -1;
        }
        else {
            System.out.println("Stack is empty");
        }

    }

    public boolean isFull(){
        if(this.top == this.stack.length - 1){
            return true;
        }
        else
            return false;
        //or
        //return (this.top == this.stack.length - 1);
    }

    public boolean isEmpty(){
        if(this.top == -1){
            return true;
        }
        else {
            return false;
        }
        //or
        //return (this.top == -1);
    }

    public void showStack(){
        System.out.println("Stack is - ");
        for (int i = 0 ; i <= this.top ; i++){
            System.out.println(this.stack[i].toString());
        }
    }
}
