package Polymorphism;

public class Animal {
    public Animal(){
        System.out.println("This is an animal");
    }

    public void makeSound(){
        System.out.println("Animal makes sound");
    }
}
