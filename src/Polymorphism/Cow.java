package Polymorphism;

public class Cow extends Animal {

    public Cow(){
        super();
    }

    @Override
    public void makeSound(){
        System.out.println("Cow makes sound");
    }
}
