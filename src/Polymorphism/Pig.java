package Polymorphism;

public class Pig extends Animal {

    public Pig(){
        super();
    }

    @Override
    public void makeSound(){
        System.out.println("Pig makes sound");
    }
}
