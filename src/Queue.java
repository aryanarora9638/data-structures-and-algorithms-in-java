public class Queue {
    private int [] queue;
    private int size;
    private int total;
    private int rear;
    private int front;

    public Queue(){
        this.size = 100;
        this.total = 0;
        this.rear = 0;
        this.front = 0;
        this.queue = new int[this.size];
    }

    public Queue(int size){
        this.size = size;
        this.total = 0;
        this.rear = 0;
        this.front = 0;
        this.queue = new int[this.size];
    }

    public void enqueue(int item){
        if(!this.isFull()){
            this.total += 1;
            this.queue[this.rear] = item;
            //normal queue
            //this.rear += 1;
            //circular queue
            this.rear = (this.rear + 1) % this.size;
        }
        else {
            System.out.println("Queue is already full");
            createNewQueue(item);
        }

    }

    private void createNewQueue(int newItem) {
        int newSize = 2 * this.size;
        Queue newQueue = new Queue(newSize);
        for(int item : this.queue){
            newQueue.enqueue(item);
        }
        newQueue.enqueue(newItem);

        this.queue = newQueue.queue;
        this.size = newQueue.size;
        this.total = newQueue.total;
        this.rear = newQueue.rear;
        this.front = newQueue.front;

        System.out.println("Increased the size of the queue 2X");
    }

    public void dequeue(){
        if(!this.isEmpty()){
            this.total -= 1;
            this.queue[this.front] = 0;
            //normal queue
            //this.front += 1;
            //circular queue
            this.front = (this.front + 1) % this.size;
        }
    }

    public boolean isEmpty(){
       if(this.total == 0){
           return true;
       }
       else
           return false;
    }

    public boolean isFull(){
        if(this.total == this.size){
            return true;
        }
        else
            return false;

    }

    public void showQueue(){
        System.out.println("Queue is - ");
        for (int item : this.queue){
            System.out.println(item);
        }
    }
}
