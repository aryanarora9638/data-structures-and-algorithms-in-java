//array implementation of a stack
public class Stack {
    private int [] stack;
    private int top;
    private int size;

    public Stack(){
        top = -1;
        size = 100;
        stack = new int[size];
    }

    public Stack(int size){
        top = -1;
        this.size = size;
        stack = new int [size];
    }

    public void push(int item){
        if (!this.isFull()){
            this.top += 1;
            this.stack[this.top] = item;
        }
        else {
            System.out.println("Stack is full already");
            createNewStack(item);
        }
    }

    private void createNewStack(int newItem) {
        Stack newStack = new Stack(this.size*2);
        for (int item : this.stack){
            newStack.push(item);
        }
        this.stack = newStack.stack;
        this.size = newStack.size;
        this.top = newStack.top;
        System.out.println("New stack created with 2x the size");
    }

    public void pop(){
        if(!this.isEmpty()){
            this.stack[this.top] = -1;
            this.top += -1;
        }
        else {
            System.out.println("Stack is empty");
        }

    }

    public boolean isFull(){
        if(this.top == this.stack.length - 1){
            return true;
        }
        else
            return false;
        //or
        //return (this.top == this.stack.length - 1);
    }

    public boolean isEmpty(){
        if(this.top == -1){
            return true;
        }
        else {
            return false;
        }
        //or
        //return (this.top == -1);
    }

    public void showStack(){
        System.out.println("Stack is - ");
        for(int item : stack){
            System.out.println(item);
        }
    }

}
