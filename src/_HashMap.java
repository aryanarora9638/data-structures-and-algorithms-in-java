public class _HashMap {
    public LinkedList[] array = new LinkedList[7];
    public int totalSize = 0;

    public _HashMap(){
        for(int i = 0; i < array.length; i++){
            array[i] = new LinkedList(0);
        }
    }

    public void addElement(Integer item){
        System.out.println("\nAdding element to - ");
        Integer hashCode = item.hashCode() % array.length;

        array[hashCode].printLinkedList();

        array[hashCode].insertAtEnd(item);
        System.out.println("\nitem added");

        array[hashCode].printLinkedList();
        totalSize++;
    }

    public void removeElement(Integer item){
        System.out.println("\nRemoving element from - ");

        Integer hashCode = item.hashCode() % array.length;
        array[hashCode].printLinkedList();

        array[hashCode].removeAtItemValue(item);

        System.out.println("\nitem removed");
        array[hashCode].printLinkedList();
        totalSize--;
    }


    public int getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(int totalSize) {
        this.totalSize = totalSize;
    }
}
